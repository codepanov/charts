<!-- Obavezan unos biblioteka -->
<script src="//www.amcharts.com/lib/4/core.js"></script>
<script src="//www.amcharts.com/lib/4/charts.js"></script>
<script src="//www.amcharts.com/lib/4/themes/animated.js"></script>
<script src="//www.amcharts.com/lib/4/themes/material.js"></script>

<!-- Ovo je element koji ce prikazivati grafikon -->
<div id="chartdiv" style="width: 900px; height 800px;"></div>

<script>

// Primeni teme animacija i osnovne boje
am4core.useTheme(am4themes_animated);
am4core.useTheme(am4themes_material);

// Chart instanca i poziv ka okruglom prikazu (PieCharts)
var chart = am4core.create("chartdiv", am4charts.PieChart);

// Kreiraj okrugli prikaz
var series = chart.series.push(new am4charts.PieSeries());

// Dodaj vrednosti i kategorije
series.dataFields.value = "ocena";
series.dataFields.category = "predmet";

// Ucitaj podatke
chart.dataSource.url = "predmeti_ocene.json";

// Zadatak_1 - uspesnost odeljenja po predmetima
/** 
* dobavi sve zakljucene ocene po predmetima u jednom odeljenju
* napravi selektor razreda i postojecih odeljenja
*/
// chart.data = [{
//   "predmet": "Matematika",
//   "ocena": 5
// }, {
//   "predmet": "Fizika",
//   "ocena": 3
// }, {
//   "predmet": "Srpski",
//   "ocena": 4
// }];

// Pozovi legendu
chart.legend = new am4charts.Legend();
</script>