<!-- Obavezan unos biblioteka -->
<script src="//www.amcharts.com/lib/4/core.js"></script>
<script src="//www.amcharts.com/lib/4/charts.js"></script>
<script src="//www.amcharts.com/lib/4/themes/animated.js"></script>
<script src="//www.amcharts.com/lib/4/themes/material.js"></script>

<!-- Ovo je element koji ce prikazivati grafikon -->
<div id="chartdiv" style="width: 900px; height 800px;"></div>

<script>

// Primeni teme animacija i osnovne boje
am4core.useTheme(am4themes_animated);
am4core.useTheme(am4themes_material);

// Chart instanca i poziv ka prikazu tipa sveca (XYChart)
var chart = am4core.create("chartdiv", am4charts.XYChart);

// Kreiraj X osu i dodeli joj kategorije
var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
categoryAxis.dataFields.category = "odeljenje";

// Kreiraj Y osu i dodeli joj vrednosti
var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
valueAxis.title.text = "Uspesnost (ocena)";

// Prikazi svece u 3D obliku
var series = chart.series.push(new am4charts.ColumnSeries3D());

// Dodaj vrednosti i kategorije
series.dataFields.valueY = "uspeh";
series.dataFields.categoryX = "odeljenje";

// Ucitaj podatke
chart.dataSource.url = "odeljenja_razredi.json";

// // Jednobojni prikaz grafikona
// series.columns.template.fill = am4core.color("#104547");

// Visebojni prikaz (nijanse) | ako zelis jednobojni, ovaj zakomentarisi 
series.heatRules.push({
 "target": series.columns.template,
 "property": "fill",
 "min": am4core.color("#cfe1ff"),
 "max": am4core.color("#7c2be0"),
 "dataField": "valueY"
});

// Razni ispisi na grafikonu
series.name = "Uspesnost odeljenja";
series.columns.template.tooltipText = "Odeljenje: {categoryX}\nUspesnost: {valueY}";

// Zadatak_2 - uspesnost odeljenja po razredima na nivou skole
/** 
* dobavi sve zakljucene ocene jednog odeljenja i izracunaj njihov prosek
* napravi selektor razreda i postojecih odeljenja
* odeljenje Prvo 1 je prvo odeljenje prvog razreda itd...
*/ 
// chart.data = [{
//   "odeljenje": "Prvo 1",
//   "uspeh": 5
// }, {
//   "odeljenje": "Prvo 2",
//   "uspeh": 3
// }, {
//   "odeljenje": "Prvo 3",
//   "uspeh": 4
// }];

// Legenda
chart.legend = new am4charts.Legend();
</script>